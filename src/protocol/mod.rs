use std::{
	io::Cursor,
	mem::size_of,
	net::{IpAddr, Ipv4Addr},
	time
};

mod peer;
pub use self::peer::BitCoinPeer;

use bytes::*;
use crypto::digest::Digest;
use crypto::sha2::Sha256;
use rand::Rng;

/// We allow dead code here, since one of those might not be used.
#[allow(dead_code)]
#[derive(Clone,Debug)]
pub enum Network {
	MainNet,
	TestNet3
}

#[derive(Debug)]
pub enum ProtocolError {
	NotEnoughData
}

// Hash the given payload twice using Sha256
fn hash_payload(payload: &[u8]) -> Vec<u8> {
	let mut sha_first = Sha256::new();
	let mut first_result: Vec<u8> = vec!();
	sha_first.input(payload);
	first_result.resize(sha_first.output_bytes(), 0);
	sha_first.result(first_result.as_mut_slice());

	let mut sha_second = Sha256::new();
	let mut second_result: Vec<u8> = vec!();
	sha_second.input(first_result.as_slice());
	second_result.resize(sha_second.output_bytes(), 0);
	sha_second.result(second_result.as_mut_slice());

	second_result
}

pub fn create_header(network: &Network, command: &str, payload: &[u8]) -> Result<Vec<u8>,&'static str> {
	let mut msg: Vec<u8> = vec!();

	// Select magic based on the selected network
	let magic = match *network {
		Network::MainNet => 0xD9B4_BEF9,
		Network::TestNet3 => 0x0709_110B,
	};

	msg.put_u32_le(magic);

	// Add command bytes; make sure that we have exactly 12 bytes
	if command.len() > 12 {
		return Err("Command to long");
	}
	let mut command_bytes = command.as_bytes().to_vec();
	while command_bytes.len() < 12 {
		command_bytes.push(0);
	}
	msg.put_slice(&command_bytes);

	// Add payload size
	msg.put_u32_le(payload.len() as u32);

	// Hash payload twice, add first 4 bytes
	let hashed_payload = hash_payload(&payload);
	msg.put_slice(&hashed_payload.as_slice()[0..4]);

	// Add payload
	msg.put_slice(&payload);

	Ok(msg)
}

fn convert_ipv4(addr: Ipv4Addr) -> Vec<u8> {
	let mut repr = vec!();
	repr.resize(10, 0);
	repr.push(0xFF);
	repr.push(0xFF);
	for octet in &addr.octets() {
		repr.push(octet.clone());
	}

	repr
}

pub fn create_version_message(network: &Network, from: &IpAddr, _to: &IpAddr) -> Result<Vec<u8>, &'static str> {
	let version: i32 = 60002;
	let services: u64 = 1;
	let timestamp: i64 = time::SystemTime::now().duration_since(time::UNIX_EPOCH).unwrap().as_secs() as i64;
	let port: u16 = (match *network {
		Network::MainNet => 8333, Network::TestNet3 => 18333
	} as u16).to_be();
	let nonce = rand::thread_rng().gen::<u64>();
	let user_agent: i8 = 0;
	let start_height: i32 = 0;

	let mut msg: Vec<u8> = vec!();
	msg.put_i32_le(version);
	msg.put_u64_le(services);
	msg.put_i64_le(timestamp);

	// Add from address
	msg.put_u64_le(services);    // Services again, according to protocol
	let ip_slice = match *from {
		IpAddr::V4(v4) => convert_ipv4(v4),
		IpAddr::V6(_) => return Err("!!! IPv6 not supported yet!")
	};
	msg.put_slice(&ip_slice);
	msg.put_u16_le(port);

	// Add "to" address
	msg.put_u64_le(services);
	let ip_slice2 = match *from {
		IpAddr::V4(v4) => convert_ipv4(v4),
		IpAddr::V6(_) => return Err("!!! IPv6 is not supported yet!")
	};
	msg.put_slice(&ip_slice2);
	msg.put_u16_le(port);

	// Additional data
	msg.put_u64_le(nonce);
	msg.put_i8(user_agent);
	msg.put_i32_le(start_height);

	create_header(&network, "version", &msg)
}

pub fn create_verack_message(network: &Network) -> Result<Vec<u8>, &'static str> {
	create_header(&network, "verack", &[])
}

pub fn create_getaddr_message(network: &Network) -> Result<Vec<u8>, &'static str> {
	create_header(&network, "getaddr", &Vec::new())
}

pub fn create_pong_message(network: &Network, nonce: u64) -> Result<Vec<u8>, &'static str> {
	let mut msg: Vec<u8> = Vec::new();
	msg.put_u64_le(nonce);
	create_header(&network, "pong", &msg)
}

pub trait ReadNumber<T> {
	fn read_number(&mut self) -> Result<T,ProtocolError>;
}

impl<'a> ReadNumber<u64> for Cursor<&'a Vec<u8>> {
	fn read_number(&mut self) -> Result<u64,ProtocolError> {
		if self.remaining() < size_of::<u64>() {
			Err(ProtocolError::NotEnoughData)
		} else {
			Ok(self.get_u64_le())
		}
	}
}

impl<'a> ReadNumber<i64> for Cursor<&'a Vec<u8>> {
	fn read_number(&mut self) -> Result<i64,ProtocolError> {
		if self.remaining() < size_of::<i64>() {
			Err(ProtocolError::NotEnoughData)
		} else {
			Ok(self.get_i64_le())
		}
	}
}

impl<'a> ReadNumber<u32> for Cursor<&'a Vec<u8>> {
	fn read_number(&mut self) -> Result<u32,ProtocolError> {
		if self.remaining() < size_of::<u32>() {
			Err(ProtocolError::NotEnoughData)
		} else {
			Ok(self.get_u32_le())
		}
	}
}

impl<'a> ReadNumber<u16> for Cursor<&'a Vec<u8>> {
	fn read_number(&mut self) -> Result<u16,ProtocolError> {
		if self.remaining() < size_of::<u16>() {
			Err(ProtocolError::NotEnoughData)
		} else {
			Ok(self.get_u16_le())
		}
	}
}

impl<'a> ReadNumber<u8> for Cursor<&'a Vec<u8>> {
	fn read_number(&mut self) -> Result<u8,ProtocolError> {
		if self.remaining() < size_of::<u8>() {
			Err(ProtocolError::NotEnoughData)
		} else {
			Ok(self.get_u8())
		}
	}
}

/// Read BitCoin varint, return as u64 for convenience
pub fn read_varint(cursor: &mut Cursor<&Vec<u8>>) -> Result<u64,ProtocolError> {
	// Try to read first byte, determine type.
	if cursor.remaining() < size_of::<u8>() {
		return Err(ProtocolError::NotEnoughData);
	}

	let len: u64 = u64::from(cursor.get_u8());

	// Parse int by type
	if len == 0xFD {
		cursor.read_number().map(|n: u16| n as u64)
	} else if len == 0xFE {
		cursor.read_number().map(|n: u32| n as u64)
	} else if len == 0xFF {
		cursor.read_number()
	} else {
		Ok(len)
	}
}

pub fn write_varint(int: u64) -> Vec<u8> {
	let mut buf = Vec::new();
	if int < 0xFD {
		buf.put_u8(int as u8);
	} else if int < 0xFFFF {
		buf.put_u8(0xFD);
		buf.put_u16_le(int as u16);
	} else if int < 0xFFFF_FFFF {
		buf.put_u8(0xFE);
		buf.put_u32_le(int as u32);
	} else {
		buf.put_u8(0xFF);
		buf.put_u64_le(int);
	}

	buf
}

pub fn create_getdata_message(network: &Network, block_hashes: Vec<Vec<u8>>) -> Result<Vec<u8>, &'static str> {
	let mut msg: Vec<u8> = Vec::new();
	let len = block_hashes.len();

	msg.put_slice(&write_varint(len as u64));

	for hash in block_hashes {
		msg.put_u32_le(2);   // 2 for DataBlock
		msg.put_slice(hash.as_slice());
		trace!("Requesting data for {}", stringutils::byte_array_to_string(hash.as_slice()));
	}

	if len > 0 {
		create_header(&network, "getdata", &msg)
	} else {
		Ok(vec!())
	}
}

pub fn create_getblocks_message(network: &Network, hashes: &[[u8; 32]], stop_hash: &[u8; 32]) -> Result<Vec<u8>, &'static str> {
	let mut msg: Vec<u8> = Vec::new();

	msg.put_u32_le(60002); // Protocol version

	if !hashes.is_empty() {
		msg.put_slice(&write_varint(hashes.len() as u64));    // Limit: 200

		for hash in hashes { msg.put_slice(&hash.into_iter().rev().cloned().collect::<Vec<u8>>()); }
	} else {
		// if the blockchain is empty, put at least one fake block there
		// this is needed for the receivers to accept this request
		msg.put_slice(&write_varint(1));
		msg.put_slice(stop_hash);
	}

	msg.put_slice(&[0u8; 32]);

	create_header(&network, "getheaders", &msg)
}
