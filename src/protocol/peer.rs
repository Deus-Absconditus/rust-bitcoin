use std::{io, net::{IpAddr,TcpStream}};

use chrono::prelude::*;

use protocol::Network;
use connection::Connection;
pub use connection::State;
use message;
use message::{Message,InvalidMessage,Service};
use protocol;

/// Messages which could not be handled by the peer.
pub struct GlobalMessages<'a> {
	peer: &'a mut BitCoinPeer,
	messages: message::Messages
}

impl<'a> GlobalMessages<'a> {
	pub fn new(data: Vec<u8>, peer: &'a mut BitCoinPeer, network: &Network) -> GlobalMessages<'a> {
		GlobalMessages {
			messages: message::Messages::new(data, network.clone()),
			peer
		}
	}
}

impl<'a> Iterator for GlobalMessages<'a> {
	type Item = Result<Message,InvalidMessage>;

	fn next(&mut self) -> Option<Self::Item> {
		let msg = self.messages.next()?; // Return None if we don't have any messages.

		// Read all available data, decode into messages
		if msg.is_ok() {
			if let Some(unhandled_msg) = self.peer.handle_message(msg.unwrap()) {
				debug!("Message was not handled: {:?}", unhandled_msg);
				Some(Ok(unhandled_msg))
			} else {
				/* Message was handled by Peer */
				None
			}
		} else {
			trace!("Got an invalid message: {:?}", msg);
			Some(msg)
		}
	}
}

/// This struct represents a remote peer on the BitCoin network
#[derive(Clone)]
pub struct BitCoinPeer {
	/// Internal connection for handling networking stuff
	connection: Connection,

	/// Last time this peer received a message
	last_message_received: Option<DateTime<Local>>,

	/// Type of Network this peer is connected to
	network: Network,

	/// Services supported by the remote end of this Peer
	services: Vec<Service>,

	/// True if we have received blocks from this peer
	transmits_blocks: bool
}

impl BitCoinPeer {
	pub fn new(remote_addr: IpAddr, my_addr: &IpAddr, network: Network) -> BitCoinPeer {
		BitCoinPeer {
			connection: Connection::new(remote_addr, &my_addr),
			last_message_received: None,
			network,
			services: Vec::new(),
			transmits_blocks: false
		}
	}

	pub fn from_stream(stream: TcpStream, network: Network) -> io::Result<BitCoinPeer> {
		Ok(BitCoinPeer {
			connection: try!(Connection::from_stream(stream, &network)),
			last_message_received: None,
			network,
			services: Vec::new(),
			transmits_blocks: false
		})
	}

	pub fn start(&mut self) {
		self.connection.start(self.network.clone());
	}

	pub fn disconnect(&mut self) {
		self.connection.disconnect();
	}

	pub fn has_data(&self) -> bool {
		self.connection.has_data()
	}

	pub fn get_connection_state(&self) -> State { self.connection.get_state() }
	pub fn get_remote_addr(&self) -> IpAddr { self.connection.get_remote_ip() }
	pub fn get_queue_size(&self) -> usize { self.connection.get_queue_size() }
	pub fn get_last_message_received(&self) -> Option<DateTime<Local>> { self.last_message_received }
	pub fn get_services(&self) -> &Vec<Service> { &self.services }

	/// Request addresses of more peers to connect to from this peer.
	/// Note that this happens asynchronously, therefore this function
	/// does not return a peer list right away.
	/// Instead the remote peer will answer with an "addr" message.
	pub fn request_peer_addresses(&mut self) {
		match protocol::create_getaddr_message(&self.network) {
			Ok(msg) => {
				match self.connection.write(&msg) {
					Ok(()) => info!("Requesting more peers via GETADDR from {}", self.connection.get_remote_ip()),
					Err(e) => error!("Could not request peers: {:?}", e)
				}

			},
			Err(e) => error!("Could not create GETADDR message: {}", e)
		};
	}

	/// Handle given message
	/// If the message cannot be handled by this peer, (e.g. an address list)
	/// it is returned
	fn handle_message(&mut self, msg: Message) -> Option<Message> {
		self.last_message_received = Some(Local::now());
		match msg {
			Message::Version {timestamp, services, ..} => {
				trace!("New version message:");
				trace!("Timestamp: {}", timestamp);

				// Assign list of services
				self.services = services;

				// Send VERACK message
				self.send_verack();
				None
			}, Message::Verack => {
				trace!("Got VERACK");

				// Forward the VERACK message to let the master know that we are connected
				Some(Message::Verack)
			}, Message::GetAddr => {
				warn!("STUB: GetAddr; TODO: Respond with peer address list");
				None
			}, Message::Ping {nonce} => {
				self.send_pong(nonce);
				None
			}, Message::Pong {..} => None,
			Message::Headers {block_headers} => {
				self.transmits_blocks = true;
				Some(Message::Headers {block_headers})
			},
			_ => Some(msg) // Forward all other messages to the BitCoin manager
		}
	}

	pub fn get_new_messages(&mut self) -> GlobalMessages {
		// Read all available data, decode into messages
		let n = self.network.clone();
		GlobalMessages::new(self.connection.read_data(), self, &n)
	}

	pub fn get_data(&mut self, block_hashes: Vec<Vec<u8>>) {
		let msg = protocol::create_getdata_message(&self.network, block_hashes);
		let data = msg.unwrap();
		if !data.is_empty() {
			if let Err(e) = self.connection.write(&data) {
				error!("Could not send GETDATA: {:?}", e);
			}
		} else {
			error!("No data message was created!");
		}
	}

	pub fn send_verack(&mut self) {
		let data = protocol::create_verack_message(&self.network).unwrap();
		if let Err(e) = self.connection.write(&data) {
			error!("Could not send VERACK: {:?}", e);
		}
	}

	/// Create & send response for PING messages
	pub fn send_pong(&mut self, nonce: u64) {
		 if let Err(e) = self.connection.write(&protocol::create_pong_message(&self.network, nonce).unwrap()) {
			 error!("Could not send PONG: {:?}", e);
		 }
	}

	pub fn request_blocks(&mut self, newest_block_hash: &[[u8; 32]], stop: &[u8; 32]) {
		if let Err(e) = self.connection.write(
			&protocol::create_getblocks_message(&self.network, newest_block_hash, stop)
		.unwrap()) {
			error!("Could not request blocks: {:?}", e);
		}
	}

	pub fn has_transmitted_blocks(&self) -> bool {
		self.transmits_blocks
	}
}
