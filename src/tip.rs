use message::Block;

/// # BlockChain Tip.
/// This struct represents the Tip of the BlockChain,
/// aka the newest 2000 non-orphan blocks.
/// 2000 blocks are needed since this is the maximum amount of entries
/// in the BitCoin ``headers`` message.
/// We cache this so we don't have to access the DB all the time.
///
/// We need to cache several blocks instead of just the newest, since
/// the timestamp of a block does not indicate it's age.
///
/// See: https://bitcoin.stackexchange.com/questions/3743/how-can-an-earlier-block-have-a-later-timestamp
///
/// Therefore we cache up to 2000 blocks, hoping that this will be enough.
pub struct Tip {
	blocks: Vec<Block>
}

#[derive(Clone,Debug,Serialize)]
pub enum TipError {
	NotEnoughBlocks,
	InvalidChain
}

#[derive(Debug)]
pub enum NewBlockError {
	IsOrphan,

	/// The current newest block on the tip is not the parent
	/// of the given block
	MissingLink
}

impl Tip {
	/// Create a new Tip instance from the given blocks
	/// Returns a new Tip instance or ``TipError::NotEnoughBlocks``
	/// if the argument slice contains less than 1 block.
	///
	/// We need at least one block to define a proper Tip, so if you
	/// don't have any, use the genesis block; see ``Block::genesis``
	pub fn new(current_tip: Vec<Block>) -> Result<Tip,TipError> {
		if current_tip.is_empty() {
			return Err(TipError::NotEnoughBlocks);
		}

		info!("Moving blocks");
		let mut tip = Tip {
			blocks: current_tip
		};

		// Sort blocks so that the newest block is @ index 0
		info!("Sorting blocks");
		tip.blocks.sort_by( |a: &Block, b: &Block| b.number.cmp(&a.number) );

		Ok(tip)
	}

	/// Add a new block to the tip. The new block must have a newer timestamp
	/// and the currently newest block must be the parent of the given block.
	pub fn add_block(&mut self, block: Block) -> Result<(),NewBlockError> {
		if block.number == -1 {
			return Err(NewBlockError::IsOrphan);
		}

		// Make sure that the block is already not part of the tip
		if block.number <= self.blocks[0].number {
			return Ok(());
		}

		if &block.prev_block != self.blocks[0].get_hash() {
			return Err(NewBlockError::MissingLink);
		}

		// Insert new block, remove oldest one if we have more than 2000 blocks.
		self.blocks.insert(0, block);
		let tip_len = self.blocks.len();

		if self.blocks.len() > 2000 {
			self.blocks.remove(tip_len - 1);
		}

		Ok(())
	}

	/// Verify if the tip is OK,
	/// that means that the blocks are in the correct order and every
	/// block is the parent of the previous one.
	/// Returns ``true`` on success or ``false`` on error.
	pub fn verify(&self) -> Result<(),TipError> {
		for i in 0..(self.blocks.len()-1) {
			let block = &self.blocks[i];
			let parent = &self.blocks[i+1];

			if block.prev_block != *parent.get_hash() {
				error!("Invalid BlockChain:\n{} should be parent of # {} {},\nBut # {} {} is before # {}",
					stringutils::byte_array_to_string(&block.prev_block),
					block.number,
					stringutils::byte_array_to_string(block.get_hash()),
					parent.number,
					stringutils::byte_array_to_string(parent.get_hash()),
					block.number);

				info!("Blocks {}...{}:", i-2, i+2);
				for j in (i-2)..(i+2) {
					info!("{}", self.blocks[j].number);
				}

				return Err(TipError::InvalidChain);
			}
		}
		Ok(())
	}

	/// Get a copy of the block with the index #1
	/// 0 is the newest block.
	pub fn clone_block_by_index(&self, index: usize) -> Option<Block> {
		self.blocks.get(index).cloned()
	}

	pub fn clone_blocks(&self) -> Vec<Block> {self.blocks.clone()}

	pub fn len(&self) -> usize {self.blocks.len()}
}
