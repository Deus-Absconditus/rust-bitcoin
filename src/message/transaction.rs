extern crate base58check;

use std::io::Cursor;

use self::base58check::ToBase58Check;
use bytes::*;
use crypto::{
	digest::Digest,
	ripemd160::Ripemd160
};

use message::header;
use protocol::*;

/// Represents the version of the bitcoin transaction
#[derive(Clone,Debug,Serialize)]
pub enum TransactionVersion {
	V1, V2
}

impl<'a> ::std::convert::From<&'a TransactionVersion> for u32 {
	fn from(version: &'a TransactionVersion) -> u32 {
		match *version { TransactionVersion::V1 => 1, TransactionVersion::V2 => 2 }
	}
}

#[derive(Clone,Debug,Serialize)]
pub struct TransactionOutpoint {
	pub hash: Vec<u8>,

	// Store as i64 since bson disallows unsigned; note that the correct type is u32
	pub index: u32
}

#[derive(Clone,Debug,Serialize)]
pub struct TransactionInput {
	pub previous_output: TransactionOutpoint,
	// script_length: VarInt -> can be read on-the-fly via: script.len()
	pub script: Vec<u8>,

	// Store as i64 since bson disallows unsigned; note that the correct type is u32
	pub sequence: u32,
}

#[derive(Clone,Debug,Serialize)]
pub struct TransactionOutput {
	pub value: i64,
	pub pk_script: Vec<u8>,
	pub address: String,
}

pub enum TxOutputError {
	PubKeyTooShort, NonStandard, Invalid
}

#[derive(Clone,Debug,Serialize)]
pub struct Transaction {
	pub version: TransactionVersion,
	pub tx_in: Vec<TransactionInput>,
	pub tx_out: Vec<TransactionOutput>,
	pub lock_time: u32,
	pub hash: Vec<u8>
}

/// Create a new transaction from scratch
impl Transaction {
	fn input_from_bytestream(mut cursor: &mut Cursor<&Vec<u8>>) -> Result<TransactionInput,ProtocolError> {
		if cursor.remaining() < 32 {
			return Err(ProtocolError::NotEnoughData);
		}
		let mut previous_output_bytes: Vec<u8> = Vec::new();
		previous_output_bytes.resize(32, 0u8);
		cursor.copy_to_slice(previous_output_bytes.as_mut_slice());
		let outpoint_index = cursor.get_u32_le();

		let script_len = read_varint(&mut cursor)?;
		if cursor.remaining() < script_len as usize {
			return Err(ProtocolError::NotEnoughData);
		}
		let mut script_bytes: Vec<u8> = Vec::new();
		script_bytes.resize(script_len as usize, 0u8);
		cursor.copy_to_slice(script_bytes.as_mut_slice());

		let sequence = cursor.read_number()?;

		Ok(TransactionInput {
			previous_output : TransactionOutpoint {
				hash: previous_output_bytes,
				index: outpoint_index
			},
			script: script_bytes,
			sequence
		})
	}

	fn address_from_hash(hash: &[u8]) -> String {
		// Create address from hash
		let mut ripe = Ripemd160::new();
		let mut ripe_result: Vec<u8> = Vec::new();
		ripe.input(&hash);
		ripe_result.resize(ripe.output_bytes(), 0u8);
		ripe.result(ripe_result.as_mut_slice());

		// Encode using base58check, this creates the final address.
		ripe_result.to_base58check(0x6F)
	}

	fn parse_tx_pubkey_pay_to_public_key_hash(bytes: &[u8], network: &Network)
			-> Result<String,TxOutputError> {
		let pubkey_hash_len = *try!(bytes.get(2).ok_or(TxOutputError::PubKeyTooShort));

		if bytes.len() < 3 + pubkey_hash_len as usize {
			return Err(TxOutputError::PubKeyTooShort);
		}

		let pubkey_hash = &bytes[3..(3+pubkey_hash_len as usize)];

		// Create address from hash
		let address = match network {
			Network::TestNet3 => pubkey_hash.to_base58check(0x6F),
			Network::MainNet => pubkey_hash.to_base58check(0x05)
		};
		trace!("TX address: {}", address);

		Ok(address)
	}

	fn parse_tx_pay_to_pubkey(bytes: &[u8]) -> Result<String,TxOutputError> {
		let pubkey_len = *bytes.get(0).unwrap() as usize;

		if bytes.len() < 1 + pubkey_len {
			return Err(TxOutputError::PubKeyTooShort);
		}

		let pubkey = &bytes[1..(1 + pubkey_len)];

		// Create address from hash
		let address = Transaction::address_from_hash(&pubkey);
		trace!("TX address: {} (From redeem hash)", address);

		Ok(address)
	}

	fn parse_tx_pay_to_script_hash(bytes: &[u8], network: &Network) -> Result<String,TxOutputError> {
		if bytes.len() < 2 {
			return Err(TxOutputError::PubKeyTooShort);
		}
		let sig_len = bytes[1];
		if bytes.len() < sig_len as usize + 2 {
			return Err(TxOutputError::PubKeyTooShort);
		}
		let redeem_hash = &bytes[2..(2+sig_len as usize)];

		// Create address from hash
		let address = match network {
			Network::TestNet3 => redeem_hash.to_base58check(0xC4),
			Network::MainNet => redeem_hash.to_base58check(0x05)
		};
		trace!("TX address: {} (From redeem hash)", address);

		Ok(address)
	}

	/// Reads the address from the script if possible
	fn parse_tx_pubkey_script(bytes: &[u8], network: &Network) -> Result<String,TxOutputError> {
		// Check for standard transaction
		if bytes.len() < 20 {
			return Err(TxOutputError::PubKeyTooShort);
		}

		// Check if transaction starts with OP_RETURN and is therefore invalid
		if bytes[0] == 0x6A {
			return Err(TxOutputError::Invalid);
		}

		// Check for standard transaction: OP_DUP OP_HASH160 == 0x76A9
		// (Pay to Public Key Hash)
		if (bytes[0] == 0x76) && (bytes[1] == 0xA9) {
			return Transaction::parse_tx_pubkey_pay_to_public_key_hash(&bytes, network);
		}

		// Check for standard transaction: OP_HASH160 == 0xA9
		// (Pay to Script Hash)
		if bytes[0] == 0xA9 {
			return Transaction::parse_tx_pay_to_script_hash(&bytes, network);
		}

		// Check if the TX is in the format Pay to Pubkey (P2PK)
		if bytes[0] < 76 { // Byte indicates data length
			return Transaction::parse_tx_pay_to_pubkey(&bytes);
		}

		Err(TxOutputError::NonStandard)
	}

	fn output_from_bytestream(mut cursor: &mut Cursor<&Vec<u8>>, network: &Network) -> Result<TransactionOutput,String> {
		let value: i64 = cursor.read_number().map_err(|_| "Not enough data".to_string())?;
		let script_len = try!(read_varint(&mut cursor).map_err(|_| "Not enough data".to_string()));

		if cursor.remaining() < script_len as usize {
			return Err("Not enough data".to_string());
		}

		let mut script_bytes: Vec<u8> = Vec::new();
		script_bytes.resize(script_len as usize, 0u8);
		cursor.copy_to_slice(script_bytes.as_mut_slice());

		let addr = match Transaction::parse_tx_pubkey_script(&script_bytes, network) {
			Ok(addr) => addr,
			Err(e) => match e {
				TxOutputError::NonStandard => {
					"non standard" // Err(format!("Non standard TX pubkey script: {}", stringutils::byte_array_to_string(&script_bytes)))
				}, TxOutputError::Invalid => return Err("Invalid TX".into()),
				TxOutputError::PubKeyTooShort => {
					"to short" // Err(format!("PubKeyScript to short: {}", stringutils::byte_array_to_string(&script_bytes)));
				}
			}.to_string()
		};

		Ok(TransactionOutput {
			value,
			address: addr,
			pk_script: script_bytes
		})
	}

	/// Try to parse a TX from the given Bytestream.
	pub fn from_cursor(cursor: &mut Cursor<&Vec<u8>>, network: &Network) -> Result<Transaction,ProtocolError> {
		// Read version
		let version: u32 = cursor.read_number()?;
		let num_inputs = read_varint(cursor)?;

		// Read all inputs...
		let mut inputs = Vec::new();
		for _ in 0..num_inputs {
			// input_from_bytestream can only return a "Not enough data" error,
			// therefore it is okay to use the ? operator here.
			inputs.push(Transaction::input_from_bytestream(cursor)?);
		}

		let num_outputs = read_varint(cursor)?;
		let mut outputs = Vec::new();
		trace!("Parsing TX output");
		for _ in 0..num_outputs {
			match Transaction::output_from_bytestream(cursor, network) {
				Ok(out) => {
					trace!("Adding TX to output vec");
					outputs.push(out);
				},
				Err(e) => trace!("Could not parse TX output: {:?}", e)
			}
		}

		let lock_time: u32 = try!(cursor.read_number());

		let mut tx = Transaction {
			version: match version {
				1 => TransactionVersion::V1, 2 => TransactionVersion::V2,
				_ => {
					trace!("Unknown TX version: {}, assuming version 2", version);
					TransactionVersion::V2
				}
			},
			tx_in : inputs,
			tx_out : outputs,
			lock_time,
			hash: Vec::new() // temp
		};

		tx.hash = header::hash_payload(&tx.to_bytestream());

		Ok(tx)
	}

	/// Try to parse the TX contained in the given Block Header.
	///
	/// This function just creates a Cursor over the bytes and
	/// calls `from_cursor`.
	pub fn from_bytestream(header: &header::Header, network: &Network) -> Result<Transaction,ProtocolError> {
		let hash = header::hash_payload(&header.payload);

		info!("       Got TX data; Hash: {}", stringutils::byte_array_to_string(&hash));

		let mut cursor = Cursor::new(&header.payload);

		let tx = Transaction::from_cursor(&mut cursor, network)?;
		if tx.hash != hash {
			trace!("TX was not built correctly; hash does not match!");
		}

		Ok(tx)
	}

	pub fn to_bytestream(&self) -> Vec<u8> {
		let mut bytes = Vec::new();

		bytes.put_u32_le( (&self.version).into());
		bytes.put_slice(&write_varint(self.tx_in.len() as u64));
		for txin in &self.tx_in {
			bytes.put_slice(&txin.previous_output.hash);
			bytes.put_u32_le(txin.previous_output.index);
			bytes.put_slice(&write_varint(txin.script.len() as u64));
			bytes.put_slice(&txin.script);
			bytes.put_u32_le(txin.sequence);
		}

		bytes.put_slice(&write_varint(self.tx_out.len() as u64));
		for txout in &self.tx_out {
			bytes.put_i64_le(txout.value);
			bytes.put_slice(&write_varint(txout.pk_script.len() as u64));
			bytes.put_slice(&txout.pk_script);
		}

		bytes.put_u32_le(self.lock_time);

		bytes
	}

	#[allow(dead_code)]
	fn create_inputs() -> Vec<TransactionInput> {
		Vec::new()
	}

	#[allow(dead_code)]
	fn create_outputs() -> Vec<TransactionOutput> {
		Vec::new()
	}

	#[allow(dead_code)]
	fn create_script() -> Vec<u8> {
		Vec::new()
	}
}
