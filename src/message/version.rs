use std::io::Cursor;
use std::net::{IpAddr, Ipv4Addr};
use std::str;
use bytes::*;

use message::{header::Header, ProtocolVersion, Message, Service};
use protocol;

fn get_protocol_version(data: i32) -> ProtocolVersion {
	match data {
		31402 => ProtocolVersion::P31402,
		60002 => ProtocolVersion::P60002,
		70012 => ProtocolVersion::P70012,
		70013 => ProtocolVersion::P70013,
		70014 => ProtocolVersion::P70014,
		70015 => ProtocolVersion::P70015,
		80002 => ProtocolVersion::P80002,
		80003 => ProtocolVersion::P80003,
		_ => {
			error!("Unknown protocol version {}", data);
			ProtocolVersion::Unknown
		}
	}
}

fn get_user_agent(mut cursor: &mut Cursor<&Vec<u8>>) -> Result<String,protocol::ProtocolError> {
	// Read bitcoin VarInt
	let len = protocol::read_varint(&mut cursor)?;

	// Read str bytes
	let mut buf: Vec<u8> = Vec::new();
	buf.resize(len as usize, 0u8);
	cursor.copy_to_slice(buf.as_mut_slice());

	Ok(String::from(str::from_utf8(&buf).unwrap()))
}

fn read_ip(cursor: &mut Cursor<&Vec<u8>>) -> IpAddr {
	cursor.advance(8); // Skip "services"
	cursor.advance(16); // Skip IP
	cursor.advance(2); // Skip Port

	IpAddr::from(Ipv4Addr::from(0))
}

/// Extract all services found in the bitfield of an incoming VERSION message
fn extract_services(bitfield: u64) -> Vec<Service> {
	let mut services = Vec::new();
	if bitfield & 0b0000_0001 == 0b0000_0001 { services.push(Service::Network); }
	if bitfield & 0b0000_0010 == 0b0000_0010 { services.push(Service::GetUtxo); }
	if bitfield & 0b0000_0100 == 0b0000_0100 { services.push(Service::Bloom); }
	if bitfield & 0b0000_1000 == 0b0000_1000 { services.push(Service::Witness); }
	if bitfield & 0b0100_0000_0000 == 0b0100_0000_0000 { services.push(Service::NetworkLimited); }

	services
}

/// Create Version message instance from bytestream found in incoming header.
pub fn from_bytestream(header: &Header) -> Message {
	let mut cursor = Cursor::new(&header.payload);

	// Get version
	let protocol_version = cursor.get_i32_le();
	let services_bitfield = cursor.get_u64_le();
	let timestamp = cursor.get_i64_le();

	// IP addresses
	let _ = read_ip(&mut cursor);
	let _ = read_ip(&mut cursor);

	cursor.advance(8); // Skip nonce

	let _user_agent = get_user_agent(&mut cursor);
	let _start_height = cursor.get_i32_le();

	Message::Version {
		protocol_version: get_protocol_version(protocol_version),
		timestamp,
		services: extract_services(services_bitfield)
	}
}
