/// All functions and structs needed to deal with message headers.
use std::{io::Cursor, str};

use bytes::*;
use crypto::{digest::Digest, sha2::Sha256};

use protocol::Network;

/// Hash the given payload.
///
/// This can be any kind of arbitrary data; at the end,
/// it produces a bitcoin-style checksum of the format
/// SHA256(SHA256(data)).
pub fn hash_payload(payload: &[u8]) -> Vec<u8> {
	let mut bytes = [0u8; 32];

	let mut sha = Sha256::new();
	sha.input(payload);
	sha.result(&mut bytes);

	sha.reset();

	sha.input(&bytes);
	sha.result(&mut bytes);
	bytes.to_vec()
}

/// Represents the Header of a Bitcoin Message
#[derive(Debug)]
pub struct Header {
	/// The data of this message, depending on the command.
	pub payload: Vec<u8>,

	/// The command of this message, e.g. "VERSION".
	pub command: String
}

/// Struct that is returned when a bitcoin message header is parsed from a bytestream
#[derive(Debug)]
pub struct HeaderParseResult {
	/// Actual Header
	pub header: Header,

	/// Additional data that was "leftover"
	/// (i.e. data that was following after the payload)
	pub additional_data: Vec<u8>
}

/// All errors which may occur during parsing of the message.
#[derive(Debug,PartialEq)]
pub enum ParseError {
	/// The header is smaller than 24 bytes
	HeaderTooSmall,

	/// The network is neither the TestNet3 nor the MainNet
	UnknownNetwork {
		network: String
	},

	/// The payload is not as big as it needs to be for this command.
	NotEnoughData,

	/// The hash of the message does not match the calculated one.
	InvalidHash,

	/// We don't know this command (yet).
	UnknownCommand {
		command: String
	}
}

impl Header {
	/// Parse a header from the given bytestream.
	///
	/// The bytestream needs to be a mutable reference, since this function
	/// extracts the first header it can find from the given stream,
	/// consuming the bytes.
	pub fn from_bytes(bytes: &mut Vec<u8>) -> Result<HeaderParseResult, ParseError> {
		// Make sure that the header is at least 24 bytes long
		if bytes.len() < 24 {
			return Err(ParseError::HeaderTooSmall);
		}

		let payload_length = {
			// Check network
			let mut cursor = Cursor::new(&bytes);
			let _network = match cursor.get_u32_le() {
				0xD9B4_BEF9 => Network::MainNet,
				0x0709_110B => Network::TestNet3,
				network => return Err(ParseError::UnknownNetwork {network: network.to_string()})
			};

			// We advance the cursor here since we copy the command later directly from the buffer.
			cursor.advance(12);

			// Read the payload length
			let payload_length = cursor.get_u32_le();

			// Make sure that we have enough data
			if bytes.len() - 24 /* 24 bytes for the header */ < payload_length as usize {
				return Err(ParseError::NotEnoughData);
			}
			payload_length
		};

		// The standard demands that commands are padded with zero bytes,
		// but we don't need this.
		// TODO: Instead of replacing the zero bytes (expensive),
		// just extract the str until the first zero byte.
		// See https://git.philippludwig.net/rust/blockchain/bitcoin-lib/issues/1.
		let command: &str = &str::from_utf8(&bytes[4..16]).unwrap().replace("\u{0}","");

		// Read payload checksum + payload
		let mut payload = bytes.split_off(24);
		let additional_data = payload.split_off(payload_length as usize);

		// Hash payload twice, compare with checksum stored
		// inside the data.
		let hashed_payload = hash_payload(&payload);
		if bytes[20..24] != hashed_payload[0..4] {
			return Err(ParseError::InvalidHash);
		}

		Ok(HeaderParseResult {
			header: Header {
				payload,
				command: command.to_ascii_lowercase()
			},
			additional_data
		})
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	/// Test with an empty buffer
	#[test]
	fn empty_buffer() {
		match Header::from_bytes(&mut Vec::new()) {
			Err(e) => assert_eq!(ParseError::HeaderTooSmall, e),
			Ok(_) => assert!(false, "from_bytes should return ParseError::HeaderTooSmall")
		};
	}

	/// Test with a buffer containing only zeroes
	#[test]
	fn zero_buffer() {
		let mut zero_buf = [0u8; 32].to_vec();
		assert!(Header::from_bytes(&mut zero_buf).is_err());
	}

	/// Test with an invalid network type
	#[test]
	fn network_type_invalid() {
		let mut buf = Vec::new();
		buf.resize(40, 0u8);
		let mut cursor = Cursor::new(buf);
		cursor.put_u32_le(0x12345678);
		cursor.put_slice(&[0u8; 32]);
		let h = Header::from_bytes(&mut cursor.into_inner());
		assert!(h.is_err());
		match h.unwrap_err() {
			ParseError::UnknownNetwork {..} => { /* As expected */ },
			e => assert!(false, "Invalid error type: {:?}", e)
		}
	}

	fn test_expected_network_type(network_type: u32) {
		let mut buf = Vec::new();
		buf.resize(40, 0u8);
		let mut cursor = Cursor::new(buf);
		cursor.put_u32_le(network_type); // MainNet indicator according to doc
		cursor.put_slice(&[0u8; 32]);
		let h = Header::from_bytes(&mut cursor.into_inner());
		assert!(h.is_err());
		assert_eq!(h.unwrap_err(), ParseError::InvalidHash);
	}

	#[test]
	fn network_type_mainnet() { test_expected_network_type(0xD9B4BEF9); }

	#[test]
	fn network_type_testnet3() { test_expected_network_type(0x0709110B); }

	#[test]
	fn hash() {
		let data = "aaa";
		let expected = "2da088f23b65eac349084e13bec32e82804801b5c4c3b6baedb8811ea517334d";
		let hashed = hash_payload(data.as_bytes());
		assert_eq!(stringutils::byte_array_to_string(&hashed), expected);
	}

	#[test]
	fn test_payload_length() {
		// Construct message with 0 payload
		let mut buf = Vec::new();
		buf.resize(40, 0u8);
		let mut cursor = Cursor::new(buf);
		cursor.put_u32_le(0x0709110B); // Network Type
		cursor.put_slice(&[0u8; 12]);   // Empty command
		cursor.put_u32_le(0); // Payload length
		let h = Header::from_bytes(&mut cursor.into_inner());
		assert!(h.is_err());
		assert_eq!(h.unwrap_err(), ParseError::InvalidHash);
	}
}
