use std::io::Cursor;

use bytes::*;

use message::block::Hash;
use message::{Message, header::Header};
use protocol;

/// All known INV message types according to the specification.
#[derive(Clone,Debug,PartialEq)]
pub enum InventoryType {
	Error, Transaction, DataBlock, FilteredBlock, CompactBlock, Unknown
}

/// Represents an INV message. Inventory messages are basically entries
/// which state that an object of the given type (e.g. a block) with the
/// given hash exists.
#[derive(Debug)]
pub struct InventoryVector {
	/// Type as specified in the message
	pub inventory_type: InventoryType,

	/// Hash
	pub hash: Hash
}

/// Parse the given bytestream to extract an INV message.
pub fn from_bytestream(header: &Header) -> Result<Message,protocol::ProtocolError> {
	let mut cursor = Cursor::new(&header.payload);

	// Read number of items
	let count: u64 = protocol::read_varint(&mut cursor)?;
	trace!("Got a new Inventory Message, containing {} items.", count);

	// Read all Inventory Vectors
	let mut hashes = Vec::new();
	for _ in 0..count {
		// Read block type
		let inv_type = match cursor.get_u32_le() {
			0 => InventoryType::Error,
			1 => InventoryType::Transaction,
			2 => InventoryType::DataBlock,
			3 => InventoryType::FilteredBlock,
			4 => InventoryType::CompactBlock,
			_ => InventoryType::Unknown
		};

		let mut hash_buf: Hash = [0u8; 32];
		cursor.copy_to_slice(&mut hash_buf);

		hashes.push( InventoryVector {
			inventory_type: inv_type,
			hash: hash_buf
		} );
	}

	Ok(Message::Inv {
		inventory_entries: hashes
	})
}
