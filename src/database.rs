extern crate mongodb;

use bson::{Bson, spec::BinarySubtype};
use self::mongodb::{Client,coll::options::FindOptions, ThreadedClient, db::ThreadedDatabase};

use message::{
	block::{Block, Hash},
	transaction::{Transaction, TransactionInput, TransactionOutput, TransactionOutpoint, TransactionVersion},
};
use protocol::Network;

#[derive(Clone,Debug,Serialize)]
pub enum DatabaseError {
	BrokenDocument,
	CouldNotFindBlock,
	EmptyChain
}

impl ::std::convert::From<bson::ValueAccessError> for DatabaseError {
	fn from(err: bson::ValueAccessError) -> DatabaseError {
		error!("Broken BlockChain: {:?}", err);
		error!("{:?}", backtrace::Backtrace::new());
		match err {
			bson::ValueAccessError::NotPresent => DatabaseError::CouldNotFindBlock,
			bson::ValueAccessError::UnexpectedType => DatabaseError::BrokenDocument
		}
	}
}

pub struct Database {
	pub collection: mongodb::coll::Collection,
}

pub type BlockResult = Result<Block,DatabaseError>;

impl Database {
	pub fn new(network: &Network, mongo_hostname: &str, port: u16) -> Database {
		info!("Connecting to DB");
		let client = Client::connect(mongo_hostname, port).unwrap();
		let coll = client.db( match network {
			Network::MainNet => "bitcoin",
			Network::TestNet3 => "bitcoin_test"
		}).collection("blocks");

		Database {
			collection: coll,
		}
	}

	fn deserialize_tx_outpoint(bson: &Bson) -> TransactionOutpoint {
		let doc = bson.as_document().unwrap();
		TransactionOutpoint {
			hash: doc.get_binary_generic("hash").unwrap().clone(),
			index: doc.get_i64("index").unwrap() as u32
		}
	}

	fn deserialize_tx_inputs(bson: &bson::Array) -> Vec<TransactionInput> {
		bson.iter()
		  .map(|serialized_input| serialized_input.as_document().unwrap())
		  .map(|doc| TransactionInput {
			script: doc.get_binary_generic("script").unwrap().clone(),
			sequence: doc.get_i64("sequence").unwrap() as u32,
			previous_output: Database::deserialize_tx_outpoint(doc.get("previous_output").unwrap())
		  })
		.collect()
	}

	fn deserialize_tx_outputs(bson: &bson::Array) -> Vec<TransactionOutput> {
		bson.iter()
		  .map(|serialized_output| serialized_output.as_document().unwrap())
		  .map(|doc| TransactionOutput {
			  value: doc.get_i64("value").unwrap(),
			  pk_script: doc.get_binary_generic("pk_script").unwrap().clone(),
			  address: String::from(doc.get_str("address").unwrap())
		  })
		.collect()
	}

	fn deserialize_txns(bson: &bson::Array) -> Vec<Transaction> {
		bson.iter().map(|serialized_tx| serialized_tx.as_document().unwrap()).map(|doc| {
			Transaction {
				version: TransactionVersion::from(doc.get("version").unwrap()),
				lock_time: doc.get_i64("lock_time").unwrap() as u32,
				hash: doc.get_binary_generic("hash").unwrap().clone(),
				tx_in: Database::deserialize_tx_inputs(doc.get_array("inputs").unwrap()),
				tx_out: Database::deserialize_tx_outputs(doc.get_array("outputs").unwrap())
			}
		}).collect()
	}

	pub fn build_block_from_doc(doc: &bson::Document) -> BlockResult {
		let mut prev_block = [0u8; 32];
		let mut hash = [0u8; 32];

		if doc.contains_key("$err") {
			error!("Broken document: {}", try!(doc.get_str("$err")));
			return Err(DatabaseError::BrokenDocument);
		}

		hash.copy_from_slice(try!(doc.get_binary_generic("_id")));
		if !doc.contains_key("prev_block") {
			warn!("Invalid block in DB: {:?}", hash);
			panic!("");
		}
		prev_block.copy_from_slice(try!(doc.get_binary_generic("prev_block")));

		let mut merkle_root = [0u8; 32];
		merkle_root.copy_from_slice(try!(doc.get_binary_generic("merkle_root")));

		Ok(Block {
			timestamp: try!(doc.get_i64("timestamp")) as u32,
			version:   try!(doc.get_i32("version")),
			bits:      try!(doc.get_i64("bits")) as u32,
			prev_block,
			nonce:     try!(doc.get_i64("nonce")) as u32,
			hash,
			merkle_root,
			txns: Database::deserialize_txns(try!(doc.get_array("txns"))),
			number: try!(doc.get_i64("number"))
		})
	}

	/// Retrieve the newest block which doesn't have a known connection
	/// to the genesis block.
	/// At the moment this is used mainly for debugging.
	pub fn get_newest_orphan(&self) -> BlockResult {
		let filter = doc!{ "number" => -1 };

		let mut options = FindOptions::default();
		options.sort = Some(doc! {"timestamp" => -1});

		let mut cursor = self.collection.find(Some(filter), Some(options)).unwrap();
		match cursor.next() {
			None => Err(DatabaseError::CouldNotFindBlock),
			Some(doc) => Database::build_block_from_doc(&doc.unwrap())
		}
	}

	/// Retrieve oldest block which doesn't have a known parent block.
	/// This is useful during the blockchain download.
	/// If we don't have any orphans, the result is None.
	pub fn get_oldest_orphan(&self) -> BlockResult {
		let filter = doc!{ "number" => -1 };

		let mut options = FindOptions::default();
		options.sort = Some(doc! {"timestamp" => 1});

		let mut cursor = self.collection.find(Some(filter), Some(options)).unwrap();
		match cursor.next() {
			None => Err(DatabaseError::CouldNotFindBlock),
			Some(doc) => Database::build_block_from_doc(&doc.unwrap())
		}
	}

	pub fn newest_block(&self) -> BlockResult {
		let mut options = FindOptions::default();
		options.sort = Some(doc! { "number" => -1 });
		let mut cursor = self.collection.find(None, Some(options)).unwrap();

		if cursor.has_next().unwrap() {
			let result = cursor.next().unwrap().unwrap();

			Database::build_block_from_doc(&result)
		} else {
			Err(DatabaseError::EmptyChain)
		}
	}

	pub fn oldest_block(&self) -> BlockResult {
		let mut options = FindOptions::default();
		options.sort = Some(doc! {"timestamp" => 1});
		let mut cursor = self.collection.find(None, Some(options)).unwrap();

		match cursor.next() {
			None => Err(DatabaseError::EmptyChain),
			Some(val) => Database::build_block_from_doc(&val.unwrap())
		}
	}

	pub fn block_by_number(&self, number: i64) -> BlockResult {
		let filter = doc!{ "number" => number };
		self.find_block_by_filter(filter)
	}

	fn find_block_by_filter(&self, filter: bson::Document) -> BlockResult {
		let mut cursor = self.collection.find(Some(filter), None).unwrap();
		match cursor.next() {
			None => Err(DatabaseError::EmptyChain),
			Some(val) => Database::build_block_from_doc(&val.unwrap())
		}
	}

	pub fn block_by_hash(&self, hash: Hash) -> BlockResult {
		let filter = doc!{"_id" => (BinarySubtype::Generic, hash.to_vec()) };
		self.find_block_by_filter(filter)
	}

	/// Remove a block from the Database.
	///
	/// **Warning:** This is a destructive operation!
	///
	/// Blocks which depend on this block are NOT marked as an orphan!
	pub fn remove_block(&self, hash: &Hash) {
		let filter = doc!{ "hash" => (BinarySubtype::Generic, hash.to_vec()) };

		self.collection.delete_one(filter, None).unwrap();
	}
}
