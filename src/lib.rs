//#![feature(test)]

#[macro_use] extern crate bson;
#[macro_use] extern crate log;
#[macro_use] extern crate serde_derive;
extern crate backtrace;
extern crate bytes;
extern crate chrono;
extern crate crypto;
extern crate rand;
extern crate stringutils;

pub mod blockchain;
pub mod connection;
pub mod database;
pub mod message;
pub mod protocol;
pub mod timer;
mod tip;

use self::tip::Tip;
pub use self::blockchain::{BlockChain, InsertError};

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
